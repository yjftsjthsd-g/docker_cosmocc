FROM alpine:3.19
LABEL maintainer "Brian Cole <docker@brianecole.com>"

ARG VERSION=3.2.3

RUN mkdir /opt/cosmocc && \
    cd /opt/cosmocc && \
    wget -q https://cosmo.zip/pub/cosmocc/cosmocc-${VERSION}.zip && \
    unzip cosmocc-${VERSION}.zip && \
    rm cosmocc-${VERSION}.zip && \
    ls && \
    : ;

ENV PATH="/opt/cosmocc/bin:${PATH}"

