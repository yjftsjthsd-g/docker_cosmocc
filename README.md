# cosmocc container image

An unofficial OCI image of cosmocc for use in CI pipelines.


## What

The cosmocc build chain for producing APE binaries, packaged in an OCI image on
top of an Alpine Linux base.


## Why

Mostly because this makes it easy to use in gitlab CI pipelines.


## Use

Run your usual build script, but with this image and variables overridden to the
cosmo tools:
```
build_ape:
  stage: build
  image: registry.gitlab.com/yjftsjthsd-g/docker_cosmocc
  variables:
    JOB_VAR: "A job variable"
    CC: "cosmocc -I/opt/cosmos/include -L/opt/cosmos/lib"
    CXX: "cosmoc++ -I/opt/cosmos/include -L/opt/cosmos/lib"
    PKG_CONFIG: "pkg-config --with-path=/opt/cosmos/lib/pkgconfig"
    INSTALL: "cosmoinstall"
    AR: "cosmoar"
  script:
    - make yourprogramhere
```


## TODOs

* Clarify paths (`/opt/cosmos`?)
* Possibly move `/opt/cosmocc` to `/opt/cosmos`
* Make images based on Debian and OpenSUSE?
* 

## License

This repo is under the MIT license (see LICENSE file), but note that that only
covers the contents of this repo (builds scripts, basically), not the contents
of the generated image!

