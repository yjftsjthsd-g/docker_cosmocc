#!/bin/sh

set -x

export FIRST_DESTINATION="--destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
if test "${CI_COMMIT_REF_NAME}" = "master"
then
    export SECOND_DESTINATION="--destination ${CI_REGISTRY_IMAGE}:latest"
else
    export SECOND_DESTINATION=""
fi

/kaniko/executor --context "${CI_PROJECT_DIR}" --dockerfile "${CI_PROJECT_DIR}/Dockerfile" ${FIRST_DESTINATION} ${SECOND_DESTINATION}
